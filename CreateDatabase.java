import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class CreateDatabase {

    private String fileFoundStatus ;
    private ArrayList<HashMap> database;

    public ArrayList<HashMap> getDatabase() {
        return database;
    }
    public String getFileStatus(){
        return fileFoundStatus;
    }

    public void setDatabase(ArrayList database) {
        this.database = database;
    }

    public CreateDatabase(String fileName) {

        this.database = new ArrayList<>();

        try {
            FileReader dataFile = new FileReader(fileName);
            Scanner input = new Scanner(dataFile);

            this.fileFoundStatus = "File Founded";

            String singleLine;
            String fieldRow = input.nextLine();
            String[] fieldRowArray = fieldRow.split(",");

            while (input.hasNext()) {

                HashMap<String, String> map = new HashMap<>();
                singleLine = input.nextLine();
                String[] singleLineArray = singleLine.split(",");

                for(int index=0; index<(fieldRowArray.length); index++){
                    if(singleLineArray.length > index){
                        map.put(fieldRowArray[index], singleLineArray[index]);
                    }
                }
                database.add(map);
            }
        }
        catch (FileNotFoundException e) {
            this.fileFoundStatus = "File Not Found";
        }
    }
}

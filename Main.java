import java.util.ArrayList;
import java.util.HashMap;


public class Main {

    public static void main(String[] args) {

        CreateDatabase matchesDatabase = new CreateDatabase("./matches.csv");
        CreateDatabase deliveriesDatabase = new CreateDatabase("./deliveries.csv");

        // Display batting and bowling analysis for match_id 1

        // Gives "First_team batting": {batsman : runs}
        //       "Second_team Batting": {batsman : runs}
        //       "First_team Bowling" : {bowler : runs_given}
        //       "Second_team Bowling" : {bowler : runs_given}

        HashMap<String, HashMap<String,Integer>> result = getAnalysis("1", deliveriesDatabase.getDatabase());

       // Number of matches played per year of all the years in IPL.
        HashMap prob1 = getTotalMatchesPerSeason(matchesDatabase.getDatabase());

       // Number of matches won of all teams over all the years of IPL
        HashMap<String ,HashMap> prob2 = getMatchesWonPerTeamPerSeason(matchesDatabase.getDatabase());

       // For the year 2016 get the extra runs conceded per team.
        HashMap prob3 = getExtraRunsPerTeam("2016", matchesDatabase.getDatabase(), deliveriesDatabase.getDatabase());

       // For the year 2015 get the top economical bowler.
        HashMap prob4 = getTopEconomicalBowler("2015", matchesDatabase.getDatabase(), deliveriesDatabase.getDatabase());

       // Number of times each team won in each venue.
        HashMap prob5 = getNumberOfTimesTeamsWonEachVenue(matchesDatabase.getDatabase());

    }

    public static HashMap<String, HashMap<String, Integer>> getAnalysis(String id, ArrayList<HashMap> deliveryArrayList) {
        HashMap<String,HashMap<String,Integer>> detailAnalysis = new HashMap<>();

        for (HashMap element: deliveryArrayList) {
            if(element.get("match_id").equals(id)) {

                if (!detailAnalysis.containsKey(element.get("batting_team")+" Batting ")) {
                    HashMap<String, Integer> player = new HashMap<>();
                    player.put((String) element.get("batsman"), (Integer.parseInt((String) element.get("batsman_runs"))));
                    detailAnalysis.put((String) element.get("batting_team")+" Batting ", player);

                } else {
                    if(!detailAnalysis.get(element.get("batting_team")+" Batting ").containsKey(element.get("batsman"))){
                        detailAnalysis.get(element.get("batting_team")+" Batting ").put((String) element.get("batsman"), (Integer.parseInt((String) element.get("batsman_runs"))));
                    }
                    else{
                        int runs = detailAnalysis.get(element.get("batting_team")+" Batting ").get(element.get("batsman")) + Integer.parseInt((String) element.get("batsman_runs"));
                        detailAnalysis.get(element.get("batting_team")+" Batting ").put((String) element.get("batsman"), runs);
                    }

                }
                if (!detailAnalysis.containsKey(element.get("bowling_team")+" Bowling ")) {
                    HashMap<String, Integer> player = new HashMap<>();
                    player.put((String) element.get("bowler"), (Integer.parseInt((String) element.get("total_runs"))));
                    detailAnalysis.put((String) element.get("bowling_team")+" Bowling ", player);

                } else {
                    if(!detailAnalysis.get(element.get("bowling_team")+" Bowling ").containsKey(element.get("bowler"))){
                        detailAnalysis.get(element.get("bowling_team")+" Bowling ").put((String) element.get("bowler"), (Integer.parseInt((String) element.get("total_runs"))));
                    }
                    else{
                        int runs = detailAnalysis.get(element.get("bowling_team")+" Bowling ").get(element.get("bowler")) + Integer.parseInt((String) element.get("total_runs"));
                        detailAnalysis.get(element.get("bowling_team")+" Bowling ").put((String) element.get("bowler"), runs);
                    }

                }
            }
        }
        return detailAnalysis;
    }


    public static HashMap getNumberOfTimesTeamsWonEachVenue(ArrayList<HashMap> matchesArrayList) {
        HashMap<String,HashMap<String,Integer>> numberOfTimesTeamsWonEachVenue = new HashMap<>();

        for (HashMap<String,String> element: matchesArrayList) {
            if(!numberOfTimesTeamsWonEachVenue.containsKey(element.get("winner"))){
                HashMap<String,Integer> value = new HashMap<>();
                value.put((String) element.get("city"), 1);
                numberOfTimesTeamsWonEachVenue.put((String) element.get("winner"), value);
            }
            else{
                if (!numberOfTimesTeamsWonEachVenue.get(element.get("winner")).containsKey(element.get("city"))){
                    numberOfTimesTeamsWonEachVenue.get(element.get("winner")).put(element.get("city"), 1);
                }
                else{
                    int times = numberOfTimesTeamsWonEachVenue.get(element.get("winner")).get(element.get("city")) + 1;
                    numberOfTimesTeamsWonEachVenue.get(element.get("winner")).put(element.get("city"), times);
                }
            }
        }
        return numberOfTimesTeamsWonEachVenue;
    }




    public static HashMap getTopEconomicalBowler(String year, ArrayList<HashMap> matchesArrayList, ArrayList<HashMap> deliveriesArrayList) {
        HashMap<String,ArrayList<HashMap>> topEconomicalBowlers = new HashMap<>();
        ArrayList matchIdArray = new ArrayList<>();

        for (HashMap element : matchesArrayList) {
            if(element.get("season").equals(year) && !matchIdArray.contains(element.get("id"))){
                matchIdArray.add(element.get("id"));
            }
        }
        for (HashMap<String ,String> element : deliveriesArrayList) {
            if (matchIdArray.contains(element.get("match_id"))) {
                if (!topEconomicalBowlers.containsKey((String) element.get("bowler"))) {
                    ArrayList value = new ArrayList();
                    HashMap<String , Integer> total_runs = new HashMap<>();
                    HashMap<String , Integer> total_balls = new HashMap<>();
                    total_runs.put("total_runs", Integer.parseInt((String) element.get("total_runs")));
                    total_balls.put("total_balls", 1);
                    value.add(total_runs);
                    value.add(total_balls);
                    topEconomicalBowlers.put((String) element.get("bowler"), value);
                }
                else {
                    int runs = (Integer) (topEconomicalBowlers.get(element.get("bowler")).get(0).get("total_runs")) + Integer.parseInt((String) element.get("total_runs"));
                    topEconomicalBowlers.get(element.get("bowler")).get(0).put("total_runs", runs);

                    int balls = (Integer) (topEconomicalBowlers.get(element.get("bowler")).get(1).get("total_balls")) + 1;
                    topEconomicalBowlers.get(element.get("bowler")).get(1).put("total_balls", balls);
                }
            }
        }
        topEconomicalBowlers.values().forEach(val->{
            HashMap<String,Double> economy = new HashMap<>();
            economy.put("Economy", Math.round(((Integer)(val.get(0).get("total_runs"))*6.0*100.0/(Integer)val.get(1).get("total_balls")))/100.0);
            val.add(economy);
        });

        double minEconomy = 9999;
        for (ArrayList<HashMap> element : topEconomicalBowlers.values()) {
            if((Double)(element.get(2).get("Economy")) < minEconomy){
                minEconomy = (Double)(element.get(2).get("Economy"));
            }
        }
        double finalMinEconomy = minEconomy;
        HashMap result = new HashMap();
        topEconomicalBowlers.forEach((k, v)->{
            if(finalMinEconomy == (Double) v.get(2).get("Economy")){
                result.put(k,v);
            }
        });
        return result;
    }




    public static HashMap getExtraRunsPerTeam(String year, ArrayList<HashMap> matchesArrayList, ArrayList<HashMap> deliveriesArrayList) {

        HashMap<String, Integer> extraRunsPerTeam = new HashMap<>();
        ArrayList<String> matchIdArray = new ArrayList<>();

        for (HashMap element : matchesArrayList) {
            if(element.get("season").equals(year) && !matchIdArray.contains(element.get("id"))){
                matchIdArray.add((String) element.get("id"));
            }
        }
        for (HashMap element: deliveriesArrayList) {
            if (matchIdArray.contains(element.get("match_id"))){
                if (!extraRunsPerTeam.containsKey(element.get("batting_team"))){
                    extraRunsPerTeam.put((String) element.get("batting_team"), Integer.parseInt((String) element.get("extra_runs")));
                }
                else{
                    int temp = extraRunsPerTeam.get(element.get("batting_team")) + Integer.parseInt((String) element.get("extra_runs"));
                    extraRunsPerTeam.put((String) element.get("batting_team"), temp);
                }
            }
        }
        return extraRunsPerTeam;
    }




    public static HashMap getMatchesWonPerTeamPerSeason(ArrayList<HashMap> matchesArrayList) {
        HashMap<String ,HashMap> matchesPerTeamPerSeason = new HashMap<>();

        for (HashMap element: matchesArrayList) {
            if(!matchesPerTeamPerSeason.containsKey(element.get("season"))){
                matchesPerTeamPerSeason.put((String) element.get("season"), new HashMap<>());
            }
            else {
                HashMap temp  = (HashMap) matchesPerTeamPerSeason.get(element.get("season"));
                if (!temp.containsKey(element.get("winner"))){
                    temp.put(element.get("winner"),1);
                }
                else {
                    int value = (int)temp.get(element.get("winner"))+1;
                    temp.put(element.get("winner"), value);
                }
            }
        }
        return matchesPerTeamPerSeason;
    }




    public static HashMap<String, Integer> getTotalMatchesPerSeason(ArrayList<HashMap> matchesArrayList) {

        HashMap<String, Integer> totalMatchesPerSeason = new HashMap<>();
        for (HashMap element : matchesArrayList) {
            if (!totalMatchesPerSeason.containsKey(element.get("season"))) {
                totalMatchesPerSeason.put((String)element.get("season"), 1);
            } else {
                int value = totalMatchesPerSeason.get((String)element.get("season")) + 1;
                totalMatchesPerSeason.put((String)element.get("season"), value);
            }
        }
        return totalMatchesPerSeason;
    }


}
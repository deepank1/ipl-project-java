import org.junit.Test;
import java.util.ArrayList;
import java.util.HashMap;
import static org.junit.Assert.*;

public class TestMain {

    @Test
    public void testCreateDatabase(){
        CreateDatabase matchesData = new CreateDatabase("./matches.csv");
        CreateDatabase deliveriesData = new CreateDatabase("./deliveries.csv");
        assertEquals("File Founded", matchesData.getFileStatus());
        assertEquals("File Founded", deliveriesData.getFileStatus());
        assertNotNull(matchesData.getDatabase());
        assertNotNull(deliveriesData.getDatabase());
        assertEquals(636, matchesData.getDatabase().size());
        assertEquals(150460, deliveriesData.getDatabase().size());
    }


    @Test
    public void testGetTotalMatchesPerSeason(){
        CreateDatabase matchesData = new CreateDatabase("./matches.csv");
        HashMap result = Main.getTotalMatchesPerSeason(matchesData.getDatabase());
        assertNotNull(result);
        assertEquals(10, result.size());
        assertEquals(58, result.get("2008"));
        assertEquals(57, result.get("2009"));
    }


    @Test
    public void testGetMatchesWonPerTeamPerSeason(){
        CreateDatabase matchesData = new CreateDatabase("./matches.csv");
        HashMap<String,HashMap> result = Main.getMatchesWonPerTeamPerSeason(matchesData.getDatabase());
        assertNotNull(result);
        assertEquals(10, result.size());
        assertEquals(8, result.get("2008").size());
        assertEquals(8, result.get("2009").size());
    }



    @Test
    public void testGetExtraRunsPerTeam(){
        CreateDatabase matchesData = new CreateDatabase("./matches.csv");
        CreateDatabase deliveriesData = new CreateDatabase("./deliveries.csv");
        HashMap result = Main.getExtraRunsPerTeam("2016", matchesData.getDatabase(), deliveriesData.getDatabase());
        assertNotNull(result);
        assertEquals(8, result.size());
        assertEquals(132, result.get("Gujarat Lions"));
    }



    @Test
    public void testGetTopEconomicalBowler(){
        CreateDatabase matchesData = new CreateDatabase("./matches.csv");
        CreateDatabase deliveriesData = new CreateDatabase("./deliveries.csv");
        HashMap<String,ArrayList<HashMap>> result = Main.getTopEconomicalBowler("2015", matchesData.getDatabase(), deliveriesData.getDatabase());
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(3.43, result.get("RN ten Doeschate").get(2).get("Economy"));
    }



    @Test
    public void getNumberOfTimesTeamsWonEachVenue(){
        CreateDatabase matchesData = new CreateDatabase("./matches.csv");
        HashMap<String,HashMap> result = Main.getNumberOfTimesTeamsWonEachVenue(matchesData.getDatabase());
        assertNotNull(result);
        assertEquals(15, result.size());
        assertEquals(45, result.get("Mumbai Indians").get("Mumbai"));
    }

}
